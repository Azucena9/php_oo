<?php
include('../Clases/Ejercicio1/Carro.php');
include('../Clases/Ejercicio1/Moto.php');
$mensajeServidor='';
$mensajeServidor1='';
$mensajeServidor2='';

//crea aqui la instancia o el objeto de la clase Moto
$moto1 = new moto;

$Carro1 = new Carro;

 if ( !empty($_POST) && isset($_POST['tipo'])){

 	//almacenamos el valor mandado por POST en el atributo color
 	$Carro1->color=$_POST['color'];
	$moto1->tipo=$_POST['tipo'];
	$moto1->tipo=$_POST['cilindros'];
 	//se construye el mensaje que sera lanzado por el servidor
 	$mensajeServidor='el servidor dice que ya escogiste un color: '. $_POST['color'];
	$mensajeServidor1='el servidor dice que eligio el tipo de moto: '. $_POST['tipo'];
	$mensajeServidor2='el servidor dice la cantidad de cilindros es : '.$_POST['cilindros'];
 	 // recibe aqui los valores mandados por post 
 }  
else{
	$mensajeServidor='el servidor dice que ya escogiste un color: '. $_POST['color'];
	$mensajeServidor1='el servidor dice que no ha elegido un tipo de moto';
	$mensajeServidor2='el servidor dice que no ha elegido cuántos cilindros tiene la moto';
}
?>

<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>
	
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor; ?>" readonly>

	<!-- aqui puedes insertar el mesaje del servidor para Moto-->
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor1; ?>" readonly>
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor2; ?>" readonly>

	<div class="container" style="margin-top: 4em">
	<header> <h1>Carro y Moto</h1></header><br>
	<form method="post">
		<div class="form-group row">

			 <label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="color" name="color" id="CajaTexto1">
			</div>
			<div class="col-sm-4">
			</div>
 			<!-- inserta aqui los inputs para recibir los atributos del objeto-->
			 <label class="col-sm-3" for="tipo">Tipo de moto:</label>		
			 <div class="form-group">
				<input type="radio" name="tipo" value="Deportiva"> Deportiva
				<br>
				<input type="radio" name="tipo" value="Scooter"> Scooter
				<br>
				<input type="radio" name="tipo" value="Chopper"> Chopper
				<br>
			 </div>
			<div class="col-sm-6">
			</div>
			<label class="col-sm-3" for="cilindros">Cilindros de la moto:</label>
			<div class="form-group">
				<select name="cilindros">
					<option value=""> - Seleccione una opción - </option>
					<option value="1">1</option>	
					<option value="2">2</option>	
					<option value="3">3</option>	
					<option value="4">4</option>	
					<option value="5">5</option>
					<option value="6">6</option>
				</select>	
			</div>	
		</div>
		<button class="btn btn-primary" type="submit" >enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>

	</div>


</body>
</html>

