<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $anioFabricacion;
	private $estadoVerificacion;
	//declaracion del método verificación
	public function verificacion(){
		if($this -> anioFabricacion <1990){
			$this->estadoVerificacion = "No";
		}
		elseif($this -> anioFabricacion >= 1990 && $this -> anioFabricacion <= 2010){
			$this->estadoVerificacion = "Revisión";
		}
		else{
			$this->estadoVerificacion = "Si";
		}	
	}
	public function get_estado(){
		$this -> verificacion();
		return $this -> estadoVerificacion;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->anioFabricacion=explode("-",$_POST['anioFabricacion'])[0];	
}




