<?php  
include_once('transporte.php');

	class Nave extends transporte{
		private $modelo;

		//sobreescritura de constructor
		public function __construct($nom,$vel,$com,$mod){
			parent::__construct($nom,$vel,$com);
			$this->modelo=$mod;
		}

		// sobreescritura de metodo
		public function resumenNave(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Modelo:</td>
						<td>'. $this->modelo.'</td>				
					</tr>';
			return $mensaje;
		}
	}

$mensaje='';

if (!empty($_POST) && $_POST['tipo_transporte']="espacial"){
    //creacion del objeto con sus respectivos parametros para el constructor
    $nave1= new nave('Enterprise','40','na','1701');
    $mensaje=$nave1->resumenNave();		
}
?>